package com.example.test.api.integration.suites;

import com.example.test.api.calls.JsonPlaceHolderAPI;
import com.example.test.api.common.RandomStringGenerator;
import com.example.test.api.data.model.jsonplaceholder.JsonPlaceHolderRequests;
import com.example.test.api.data.model.jsonplaceholder.JsonPlaceHolderResponse;
import com.example.test.api.integration.asserts.CommonAssert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class JsonPlaceHolderTests {

    public static JsonPlaceHolderResponse response;
    @BeforeClass
    public void prepareData(){
        response = JsonPlaceHolderAPI.jsonPlaceHolderCreatePost(new JsonPlaceHolderRequests(
                RandomStringGenerator.createRandomWholeNumberInteger(0,5),
                RandomStringGenerator.createRandomWholeNumberInteger(0,5),
                RandomStringGenerator.createRandomStringAlphabeticWithLen(6),
                RandomStringGenerator.createRandomStringAlphabeticWithLen(6)));
    }
    @Test
    public void verifyListOfPosts(){
        new CommonAssert().assertResponseStructure(JsonPlaceHolderAPI.verifyJsonPlaceHolderPosts());
    }
    @Test
    public void verifySinglePost(){
    new CommonAssert().assertResponseStructure(JsonPlaceHolderAPI.verifyJsonPlaceHolderSinglePost(RandomStringGenerator.createRandomWholeNumberInteger(1,5)));
    }
    @Test
    public void verifyComments(){
        new CommonAssert().assertResponseStructure(JsonPlaceHolderAPI.verifyJsonPlaceHolderSingleUserComments(response.getUserId()));
    }
    @Test
    public void verifyPostIdComments(){
        new CommonAssert().assertResponseStructure(JsonPlaceHolderAPI.verifyJsonPlaceHolderCommentsFromPostID(response.getId()));
    }
}