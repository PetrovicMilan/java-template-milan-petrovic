package com.example.test.api.integration.suites;

import com.example.test.api.calls.CrocodilesAPI;
import com.example.test.api.common.RandomStringGenerator;
import com.example.test.api.integration.asserts.CommonAssert;

import org.testng.annotations.Test;

public class CrocodileTests{
    @Test
    public void verifyListOfCrocodiles(){
        new CommonAssert().assertResponseStructure(CrocodilesAPI.validateListOfCrocodiles());
    }
    @Test
    public void verifySingleCrocodile(){
        new CommonAssert().assertResponseStructure(CrocodilesAPI.validateSingleCrocodile(RandomStringGenerator.createRandomWholeNumberInteger(1,5)));
    }
}
