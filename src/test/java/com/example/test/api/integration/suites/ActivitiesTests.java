package com.example.test.api.integration.suites;

import com.example.test.api.calls.ActivitiesAPI;
import com.example.test.api.common.RandomStringGenerator;
import com.example.test.api.data.model.fakerestapi.ActivityRequest;
import com.example.test.api.data.model.fakerestapi.ActivityResponse;
import com.example.test.api.integration.asserts.CommonAssert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ActivitiesTests
{
    public static ActivityResponse response;
    @BeforeClass
    public void prepareData()
    {
        response = ActivitiesAPI.createNewActivity(new ActivityRequest(
                RandomStringGenerator.createRandomWholeNumberInteger(1,10),
                RandomStringGenerator.createRandomStringWithLen(8),
                RandomStringGenerator.createCurrentDateAsString(),
                RandomStringGenerator.createRandomBool()));
    }
    @Test
    public void verifyGetAllActivities() {
        new CommonAssert().assertResponseStructure(ActivitiesAPI.getAllActivities());
    }

    @Test(testName = "DefinedIDActivity")
    public void verifyGetDefinedIDActivity(){

        new CommonAssert().assertResponseStructure(ActivitiesAPI.getActivitiesDefinedID(response.getId()));
    }
}
