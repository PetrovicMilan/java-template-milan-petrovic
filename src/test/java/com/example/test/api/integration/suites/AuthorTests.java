package com.example.test.api.integration.suites;

import com.example.test.api.calls.AuthorAPI;
import com.example.test.api.common.RandomStringGenerator;
import com.example.test.api.data.model.fakerestapi.AuthorRequest;
import com.example.test.api.data.model.fakerestapi.AuthorResponse;
import com.example.test.api.integration.asserts.CommonAssert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AuthorTests {

        public static AuthorResponse response;
        @BeforeClass
        public void prepareData() {
            response = AuthorAPI.createNewAuthor(new AuthorRequest(
                    RandomStringGenerator.createRandomWholeNumberInteger(1,100),
                    RandomStringGenerator.createRandomWholeNumberInteger(1,100),
                    RandomStringGenerator.createRandomStringWithLen(6),
                    RandomStringGenerator.createRandomStringWithLen(6)));
        }

        @Test
        public void verifyGetAuthors() {
            new CommonAssert().assertResponseStructure(AuthorAPI.verifyAuthors());
        }

        @Test
        public void verifyBookID() {
            new CommonAssert().assertResponseStructure(AuthorAPI.verifyBookID(response.getId()));
        }

        @Test
        public void verifyAuthorID() {
            new CommonAssert().assertResponseStructure(AuthorAPI.verifySingleAuthor(response.getId()));
        }

        @Test
        public void updateSingleAuthor() {
            AuthorAPI.updateAuthor(new AuthorRequest(
                    RandomStringGenerator.createRandomWholeNumberInteger(0,20),
                    RandomStringGenerator.createRandomWholeNumberInteger(0,100),
                    RandomStringGenerator.createRandomStringWithLen(5),
                    RandomStringGenerator.createRandomStringWithLen(6)));
        }

        @AfterClass
        public void deleteSingleUser() {
            AuthorAPI.deleteAuthor(response.getId());
        }
    }