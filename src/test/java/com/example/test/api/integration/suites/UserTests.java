package com.example.test.api.integration.suites;

import com.example.test.api.calls.UserListAPI;
import com.example.test.api.common.init.TestBase;
import com.example.test.api.common.RandomStringGenerator;
import com.example.test.api.data.model.users.*;
import com.example.test.api.integration.asserts.CommonAssert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class UserTests extends TestBase {
    private static CreateUserResponse response;
    @BeforeClass
    public void prepareData()
    {
        response = UserListAPI.createSingleUser(new CreateUserRequest(
                RandomStringGenerator.createRandomStringAlphabeticWithLen(6),
                RandomStringGenerator.createRandomStringAlphabeticWithLen(4)));

        UserListAPI.updateSingleUser(new CreateUserRequest(
                RandomStringGenerator.createRandomStringAlphabeticWithLen(6),
                RandomStringGenerator.createRandomStringAlphabeticWithLen(5)),
                RandomStringGenerator.createRandomWholeNumberInteger(0,1000));

        UserListAPI.registrationRequest(new RegistrationRequest(
                RandomStringGenerator.createRandomStringAlphabeticWithLen(6),
                RandomStringGenerator.createRandomStringAlphabeticWithLen(5)));

        UserListAPI.loginResponse(new LoginRequest(
                RandomStringGenerator.createRandomStringAlphabeticWithLen(6),
                RandomStringGenerator.createRandomStringAlphabeticWithLen(5)));
    }
    @Test
    public void verifyListOfUsers() {
        new CommonAssert().assertResponseStructure(UserListAPI.validateGetListOfUsers());
    }
    @Test
    public void verifySingleUser()
    {
        new CommonAssert().assertResponseStructure(UserListAPI.validateGetSingleUser(RandomStringGenerator.createRandomWholeNumberInteger(1,10)));
    }
    @Test
    public void verifyListOfResources()
    {
        new CommonAssert().assertResponseStructure(UserListAPI.validateListOfResources());
    }
    @Test
    public void verifyOneResource(){
        new CommonAssert().assertResponseStructure(UserListAPI.validateSingleResource());
    }
    }
