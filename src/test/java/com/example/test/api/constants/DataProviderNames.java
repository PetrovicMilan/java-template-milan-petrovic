package com.example.test.api.constants;

public class DataProviderNames {
    //Crocodiles examples
    public static final String VERIFY_EXAMPLE = "verifyExample";
    public static final String VERIFY_REGISTER_NEW_USER="verifyRegisterNewUser";
    public static final String VERIFY_CREATE_CROCODILE="verifyCreateCrocodile";
    public static final String VERIFY_UPDATE_CROCODILE="verifyUpdateCrocodile";
    public static final String VERIFY_PATCH_CROCODILE ="verifyPatchCrocodile";
    public static final String VERIFY_DELETE_CROCODILE="verifyDeleteCrocodile";

    //Users examples
    public static final String VERIFY_CREATE_USER="verifyCreateUser";
    public static final String VERIFY_UPDATE_USER="verifyUpdateUser";
    public static final String VERIFY_PATCH_UPDATE_USER="verifyPatchUpdateUser";
    public static final String VERIFY_DELETE_USER="verifyDeleteUser";

    //FakeRestApi examples
    public static final String VERIFY_NEW_ACTIVITY="verifyNewActivity";
    public static final String VERIFY_UPDATING_ACTIVITY="verifyUpdatingActivity";
    public static final String VERIFY_DELETE_ACTIVITY="verifyDeleteActivity";
}
