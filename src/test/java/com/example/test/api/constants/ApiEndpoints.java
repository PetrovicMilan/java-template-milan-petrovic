package com.example.test.api.constants;

//List of endpoint as constants
public class ApiEndpoints {

    //Users
    public static final String GET_LIST_OF_USERS = "https://reqres.in/api/users?page=2";
    public static final String GET_AND_DELETE_SINGLE_USER(Integer id) {
        return "https://reqres.in/api/users/" + id;
    }
    public static final String GET_LIST_OF_RESOURCES="https://reqres.in/api/unknown";
    public static final String GET_ONE_RESOURCE="https://reqres.in/api/unknown/2";
    public static final String CREATE_SINGLE_USER ="https://reqres.in/api/users";
    public static final String UPDATE_USER(Integer id){
        return "https://reqres.in/api/users/" + id;
    }
    public static final String REGISTER_SUCCESSFUL="https://reqres.in/api/register";
    public static final String LOGIN_SUCCESSFUL="https://reqres.in/api/login";


    //Crocodiles
    public static final String GET_LIST_OF_CROCODILES="https://test-api.k6.io/public/crocodiles/";
    public static final String GET_SINGLE_CROCODILE(Integer id){
        return "https://test-api.k6.io/my/crocodiles/" + id + "/";
    }
    public static final String REGISTER_CROCODILE_USER ="https://test-api.k6.io/user/register/";
    public static final String GET_AND_CREATE_CROCODILES ="https://test-api.k6.io/my/crocodiles/";
    public static final String UPDATE_AND_DELETE_SINGLE_CROCODILE(Integer id){
        return "https://test-api.k6.io/my/crocodiles/" + id +"/";}
    public static final String LOGIN_TOKEN="https://test-api.k6.io/auth/token/login/";

    //JsonPlaceHolder
    public static final String JSON_PLACEHOLDER_GETPOSTS="https://jsonplaceholder.typicode.com/posts";
    public static final String JSON_PLACEHOLDER_GETSINGLEPOST(Integer id){
        return "https://jsonplaceholder.typicode.com/posts/" + id;
    }
    public static final String JSON_PLACEHOLDER_GETSINGLEUSERSCOMMENTS(Integer id){
        return "https://jsonplaceholder.typicode.com/posts/" + id + "/comments";
    }
    public static final String JSON_PLACEHOLDER_GETCOMMENTSPOSTID(Integer POSTID){
        return "https://jsonplaceholder.typicode.com/comments?postId=" + POSTID;
    }

    //FakeRestApi
    public static final String FAKE_REST_API_ACTIVITIES="https://fakerestapi.azurewebsites.net/api/v1/Activities";

    public static final String FAKE_REST_API_ACTIVITIESID(int id){
        return "https://fakerestapi.azurewebsites.net/api/v1/Activities/" + id;
    }
    public static final String AUTHORS="https://fakerestapi.azurewebsites.net/api/v1/Authors";
    public static final String AUTHOR_BOOK_ID(Integer ID){
        return "https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/" + ID;
    }
    public static final String AUTHOR_ID(Integer ID){
        return "https://fakerestapi.azurewebsites.net/api/v1/Authors/" + ID;
    }
}
