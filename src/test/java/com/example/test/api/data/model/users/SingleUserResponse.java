package com.example.test.api.data.model.users;

import com.example.test.api.annotations.ResponseRequiredField;
import com.example.test.api.data.model.common.Data;
import com.example.test.api.data.model.common.ErrResponse;
import com.example.test.api.data.model.common.Support;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Objects;

public class SingleUserResponse extends ErrResponse implements Serializable {

    @SerializedName("data")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Data data;
    @SerializedName("support")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Support support;


    public SingleUserResponse() {
    }

    public SingleUserResponse(Data data, Support support) {
        super();
        this.data = data;
        this.support = support;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Support getSupport() {
        return support;
    }

    public void setSupport(Support support) {
        this.support = support;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(SingleUserResponse.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("data");
        sb.append('=');
        sb.append(((this.data == null)?"<null>":this.data));
        sb.append(',');
        sb.append("support");
        sb.append('=');
        sb.append(((this.support == null)?"<null>":this.support));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.data == null)? 0 :this.data.hashCode()));
        result = ((result* 31)+((this.support == null)? 0 :this.support.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SingleUserResponse) == false) {
            return false;
        }
        SingleUserResponse rhs = ((SingleUserResponse) other);
        return ((Objects.equals(this.data, rhs.data))&&((this.support == rhs.support)||((this.support!= null)&&this.support.equals(rhs.support))));
    }

}