package com.example.test.api.data.model.fakerestapi;

public class ActivityResponse extends Activity{
    public void parseCreateNewActivity(ActivityRequest activityRequest){
        setId(activityRequest.getId());
        setTitle(activityRequest.getTitle());
        setCompleted(activityRequest.getCompleted());
        setDueDate(activityRequest.getDueDate());
    }
}
