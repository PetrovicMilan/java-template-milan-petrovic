package com.example.test.api.data.model.crocodiles;


import com.example.test.api.annotations.ResponseRequiredField;
import com.example.test.api.data.model.common.ErrResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TokenResponse extends ErrResponse implements Serializable {

    @SerializedName("refresh")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String refresh;
    @SerializedName("access")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String access;


    public TokenResponse() {
    }


    public TokenResponse(String refresh, String access) {
        super();
        this.refresh = refresh;
        this.access = access;
    }

    public String getRefresh() {
        return refresh;
    }

    public void setRefresh(String refresh) {
        this.refresh = refresh;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(TokenResponse.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("refresh");
        sb.append('=');
        sb.append(((this.refresh == null)?"<null>":this.refresh));
        sb.append(',');
        sb.append("access");
        sb.append('=');
        sb.append(((this.access == null)?"<null>":this.access));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}