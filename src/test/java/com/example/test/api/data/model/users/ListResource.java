package com.example.test.api.data.model.users;

import java.io.Serializable;
import java.util.List;

import com.example.test.api.annotations.ResponseRequiredField;
import com.example.test.api.data.model.common.Datum;
import com.example.test.api.data.model.common.ErrResponse;
import com.example.test.api.data.model.common.Support;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListResource extends ErrResponse implements Serializable {

    @SerializedName("page")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer page;
    @SerializedName("per_page")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer perPage;
    @SerializedName("total")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer total;
    @SerializedName("total_pages")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer totalPages;
    @SerializedName("data")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private List<Datum> data = null;
    @SerializedName("support")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Support support;


    public ListResource() {
    }


    public ListResource(Integer page, Integer perPage, Integer total, Integer totalPages, List<Datum> data, Support support) {
        super();
        this.page = page;
        this.perPage = perPage;
        this.total = total;
        this.totalPages = totalPages;
        this.data = data;
        this.support = support;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPerPage() {
        return perPage;
    }

    public void setPerPage(Integer perPage) {
        this.perPage = perPage;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Support getSupport() {
        return support;
    }

    public void setSupport(Support support) {
        this.support = support;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(ListResource.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("page");
        sb.append('=');
        sb.append(((this.page == null)?"<null>":this.page));
        sb.append(',');
        sb.append("perPage");
        sb.append('=');
        sb.append(((this.perPage == null)?"<null>":this.perPage));
        sb.append(',');
        sb.append("total");
        sb.append('=');
        sb.append(((this.total == null)?"<null>":this.total));
        sb.append(',');
        sb.append("totalPages");
        sb.append('=');
        sb.append(((this.totalPages == null)?"<null>":this.totalPages));
        sb.append(',');
        sb.append("data");
        sb.append('=');
        sb.append(((this.data == null)?"<null>":this.data));
        sb.append(',');
        sb.append("support");
        sb.append('=');
        sb.append(((this.support == null)?"<null>":this.support));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.total == null)? 0 :this.total.hashCode()));
        result = ((result* 31)+((this.perPage == null)? 0 :this.perPage.hashCode()));
        result = ((result* 31)+((this.data == null)? 0 :this.data.hashCode()));
        result = ((result* 31)+((this.totalPages == null)? 0 :this.totalPages.hashCode()));
        result = ((result* 31)+((this.page == null)? 0 :this.page.hashCode()));
        result = ((result* 31)+((this.support == null)? 0 :this.support.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ListResource) == false) {
            return false;
        }
        ListResource rhs = ((ListResource) other);
        return (((((((this.total == rhs.total)||((this.total!= null)&&this.total.equals(rhs.total)))&&((this.perPage == rhs.perPage)||((this.perPage!= null)&&this.perPage.equals(rhs.perPage))))&&((this.data == rhs.data)||((this.data!= null)&&this.data.equals(rhs.data))))&&((this.totalPages == rhs.totalPages)||((this.totalPages!= null)&&this.totalPages.equals(rhs.totalPages))))&&((this.page == rhs.page)||((this.page!= null)&&this.page.equals(rhs.page))))&&((this.support == rhs.support)||((this.support!= null)&&this.support.equals(rhs.support))));
    }

}
