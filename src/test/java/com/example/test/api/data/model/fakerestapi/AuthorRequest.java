package com.example.test.api.data.model.fakerestapi;

public class AuthorRequest extends Author{
    public AuthorRequest(Integer id, Integer idBook, String firstName, String lastName) {
        super(id, idBook, firstName, lastName);
    }
}
