package com.example.test.api.data.model.crocodiles;

import com.example.test.api.annotations.ResponseRequiredField;
import com.example.test.api.data.model.common.ErrResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ListCrocodile extends ErrResponse implements Serializable {

    @SerializedName("id")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer id;
    @SerializedName("name")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String name;
    @SerializedName("sex")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String sex;
    @SerializedName("date_of_birth")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String dateOfBirth;
    @SerializedName("age")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer age;

    public ListCrocodile() {
    }

    public ListCrocodile(Integer id, String name, String sex, String dateOfBirth, Integer age) {
        super();
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.dateOfBirth = dateOfBirth;
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(ListCrocodile.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null)?"<null>":this.id));
        sb.append(',');
        sb.append("name");
        sb.append('=');
        sb.append(((this.name == null)?"<null>":this.name));
        sb.append(',');
        sb.append("sex");
        sb.append('=');
        sb.append(((this.sex == null)?"<null>":this.sex));
        sb.append(',');
        sb.append("dateOfBirth");
        sb.append('=');
        sb.append(((this.dateOfBirth == null)?"<null>":this.dateOfBirth));
        sb.append(',');
        sb.append("age");
        sb.append('=');
        sb.append(((this.age == null)?"<null>":this.age));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }
}