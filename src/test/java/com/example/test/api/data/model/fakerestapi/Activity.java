package com.example.test.api.data.model.fakerestapi;

import com.example.test.api.annotations.ResponseRequiredField;
import com.example.test.api.data.model.common.ErrResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Activity extends ErrResponse implements Serializable {

    @SerializedName("id")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer id;
    @SerializedName("title")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String title;
    @SerializedName("dueDate")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String dueDate;
    @SerializedName("completed")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Boolean completed;

    public Activity() {
    }

    public Activity(Integer id, String title, String dueDate, Boolean completed) {
        super();
        this.id = id;
        this.title = title;
        this.dueDate = dueDate;
        this.completed = completed;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Activity.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null)?"<null>":this.id));
        sb.append(',');
        sb.append("title");
        sb.append('=');
        sb.append(((this.title == null)?"<null>":this.title));
        sb.append(',');
        sb.append("dueDate");
        sb.append('=');
        sb.append(((this.dueDate == null)?"<null>":this.dueDate));
        sb.append(',');
        sb.append("completed");
        sb.append('=');
        sb.append(((this.completed == null)?"<null>":this.completed));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}
