package com.example.test.api.data.model.jsonplaceholder;

public class JsonPlaceHolderRequests extends JsonPlaceHolderPosts{
    public JsonPlaceHolderRequests(Integer userId, Integer id, String title, String body) {
        super(userId, id, title, body);
    }
}

