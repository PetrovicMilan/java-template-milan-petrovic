package com.example.test.api.data.model.common;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class ErrResponse implements Serializable
{
    public ErrResponse() {
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this).toString();
    }
}