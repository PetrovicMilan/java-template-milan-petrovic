package com.example.test.api.data.model.users;
import com.example.test.api.annotations.ResponseRequiredField;
import com.example.test.api.data.model.common.Datum;
import com.example.test.api.data.model.common.ErrResponse;
import com.example.test.api.data.model.common.Support;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SingleResource extends ErrResponse implements Serializable {

    @SerializedName("data")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Datum data;
    @SerializedName("support")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Support support;


    public SingleResource() {
    }

    public SingleResource(Datum data, Support support) {
        super();
        this.data = data;
        this.support = support;
    }

    public Datum getData() {
        return data;
    }

    public void setData(Datum data) {
        this.data = data;
    }

    public Support getSupport() {
        return support;
    }

    public void setSupport(Support support) {
        this.support = support;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(SingleResource.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("data");
        sb.append('=');
        sb.append(((this.data == null)?"<null>":this.data));
        sb.append(',');
        sb.append("support");
        sb.append('=');
        sb.append(((this.support == null)?"<null>":this.support));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }
}