package com.example.test.api.data.model.fakerestapi;

public class ActivityRequest extends Activity{
    public ActivityRequest(Integer id, String title, String dueDate, Boolean completed) {
        super(id, title, dueDate, completed);
    }
}
