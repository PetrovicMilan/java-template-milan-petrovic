package com.example.test.api.data.model.crocodiles;

import com.example.test.api.annotations.ResponseRequiredField;
import com.example.test.api.data.model.common.ErrResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RegisterCrocodileResponse extends ErrResponse implements Serializable {

    @SerializedName("username")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String username;
    @SerializedName("first_name")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String lastName;
    @SerializedName("email")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String email;


    public RegisterCrocodileResponse() {
    }


    public RegisterCrocodileResponse(String username, String firstName, String lastName, String email) {
        super();
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void parseCreateUserResponse(CrocodilesRequest crocodilesRequest){
            setEmail(crocodilesRequest.getEmail());
            setFirstName(crocodilesRequest.getFirstName());
            setUsername(crocodilesRequest.getUsername());
            setLastName(crocodilesRequest.getLastName());
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(RegisterCrocodileResponse.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("username");
        sb.append('=');
        sb.append(((this.username == null)?"<null>":this.username));
        sb.append(',');
        sb.append("firstName");
        sb.append('=');
        sb.append(((this.firstName == null)?"<null>":this.firstName));
        sb.append(',');
        sb.append("lastName");
        sb.append('=');
        sb.append(((this.lastName == null)?"<null>":this.lastName));
        sb.append(',');
        sb.append("email");
        sb.append('=');
        sb.append(((this.email == null)?"<null>":this.email));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}