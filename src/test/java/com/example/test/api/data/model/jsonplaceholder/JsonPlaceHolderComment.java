package com.example.test.api.data.model.jsonplaceholder;
import com.example.test.api.annotations.ResponseRequiredField;
import com.example.test.api.data.model.common.ErrResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class JsonPlaceHolderComment extends ErrResponse implements Serializable {

    @SerializedName("postId")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer postId;
    @SerializedName("id")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer id;
    @SerializedName("name")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String name;
    @SerializedName("email")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String email;
    @SerializedName("body")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String body;


    public JsonPlaceHolderComment() {
    }

    public JsonPlaceHolderComment(Integer postId, Integer id, String name, String email, String body) {
        super();
        this.postId = postId;
        this.id = id;
        this.name = name;
        this.email = email;
        this.body = body;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(JsonPlaceHolderComment.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("postId");
        sb.append('=');
        sb.append(((this.postId == null)?"<null>":this.postId));
        sb.append(',');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null)?"<null>":this.id));
        sb.append(',');
        sb.append("name");
        sb.append('=');
        sb.append(((this.name == null)?"<null>":this.name));
        sb.append(',');
        sb.append("email");
        sb.append('=');
        sb.append(((this.email == null)?"<null>":this.email));
        sb.append(',');
        sb.append("body");
        sb.append('=');
        sb.append(((this.body == null)?"<null>":this.body));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }
}