package com.example.test.api.data.provider;

import com.example.test.api.common.RandomStringGenerator;
import com.example.test.api.common.ValueChoosers;
import com.example.test.api.constants.DataProviderNames;
import com.example.test.api.data.model.users.CreateUserRequest;
import org.testng.annotations.DataProvider;

public class UsersProvider {
    @DataProvider(name = DataProviderNames.VERIFY_CREATE_USER)
    public Object[][] verifyCreateUser(){
        return new Object[][]{
                {"FirstUser", this.prepareCreateUser()},
                {"SecondUser", this.prepareCreateUser()}
        };
    }
    @DataProvider(name = DataProviderNames.VERIFY_UPDATE_USER)
    public Object[][] verifyUpdateUser(){
        return new Object[][]{
                {"firstUpdatedUser", this.prepareCreateUser(), RandomStringGenerator.createRandomWholeNumberInteger(1,10)},
                {"secondUpdatedUser", this.prepareCreateUser(), RandomStringGenerator.createRandomWholeNumberInteger(1,10)}
        };
    }
    @DataProvider(name = DataProviderNames.VERIFY_PATCH_UPDATE_USER)
    public Object[][] verifyPatchUpdateUser(){
        return new Object[][]{
                {"firstPatchUser", this.prepareUpdateUserName(), RandomStringGenerator.createRandomWholeNumberInteger(1,5)},
                {"secondPatchUser", this.prepareUpdateUserJob(), RandomStringGenerator.createRandomWholeNumberInteger(1,5)}
        };
    }
    @DataProvider(name = DataProviderNames.VERIFY_DELETE_USER)
    public Object[][] verifyDeleteUser(){
        return new Object[][]{
                {"firstDeletedUser", this.prepareCreateUser()},
                {"secondDeletedUser", this.prepareCreateUser()}
        };
    }
    public CreateUserRequest prepareCreateUser(){
        return new CreateUserRequest(RandomStringGenerator.createRandomStringAlphabeticWithLen(6), ValueChoosers.getRandomPosition());
    }
    public CreateUserRequest prepareUpdateUserName(){
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setName(RandomStringGenerator.createRandomStringAlphabeticWithLen(6));
        return createUserRequest;
    }
    public CreateUserRequest prepareUpdateUserJob(){
        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setJob(ValueChoosers.getRandomPosition());
        return createUserRequest;
    }
}
