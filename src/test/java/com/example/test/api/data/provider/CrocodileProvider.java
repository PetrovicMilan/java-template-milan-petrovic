package com.example.test.api.data.provider;

import com.example.test.api.calls.CrocodilesAPI;
import com.example.test.api.common.ValueChoosers;
import com.example.test.api.constants.DataProviderNames;
import com.example.test.api.common.RandomStringGenerator;
import com.example.test.api.data.model.crocodiles.*;
import com.example.test.api.environment.ConfigSetup;
import org.testng.annotations.DataProvider;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class CrocodileProvider {
    @DataProvider(name = DataProviderNames.VERIFY_CREATE_CROCODILE)
    public Object[][] verifyCreateCrocodile() {
        return new Object[][]{
                {"FirstUser", this.prepareNewCrocodileRequest(), this.prepareCreateToken()},
                {"SecondUser", this.prepareNewCrocodileRequest(), this.prepareCreateToken()}
        };
    }
    @DataProvider(name = DataProviderNames.VERIFY_REGISTER_NEW_USER)
    public Object[][] verifyRegisterNewUser() {
        return new Object[][]{
                {"FistNewUser", this.prepareCreateNewUser()},
                {"SecondNewUser", this.prepareCreateNewUser()}
        };
    }
    @DataProvider(name = DataProviderNames.VERIFY_DELETE_CROCODILE)
    public Object[][] verifyDeleteCrocodile() {
        return new Object[][]{
                {"firstUser", this.prepareNewCrocodileRequest(), this.prepareCreateToken()},
                {"secondUser", this.prepareNewCrocodileRequest(), this.prepareCreateToken()}
        };
    }

    @DataProvider(name = DataProviderNames.VERIFY_PATCH_CROCODILE)
    public Object[][] verifyUpdateCrocodileWithPatch() {
        return new Object[][]{
                {"firstUpdatedCrocodile", this.prepareNewCrocodileRequest(), this.prepareCreateToken(), this.prepareUpdateCrocodileName()},
                {"SecondUpdatedCrocodile",this.prepareNewCrocodileRequest(), this.prepareCreateToken(), this.prepareUpdateCrocodileSex()},
                {"ThirdUpdatedCrocodile",this.prepareNewCrocodileRequest(), this.prepareCreateToken(), this.prepareUpdateCrocodileDateOfBirth()},
        };
    }

    @DataProvider(name = DataProviderNames.VERIFY_UPDATE_CROCODILE)
    public Object[][] verifyUpdateCrocodile() {
        return new Object[][]{
                {"firstUpdatedUser", this.prepareNewCrocodileRequest(), this.prepareCreateToken(), this.prepareUpdateCrocodile()},
                {"secondUpdatedUser", this.prepareNewCrocodileRequest(), this.prepareCreateToken(), this.prepareUpdateCrocodile()}
        };
    }
    public CreateNewCrocodileRequest prepareNewCrocodileRequest() {
        return new CreateNewCrocodileRequest(RandomStringGenerator.createRandomStringAlphabeticWithLen(6),
                ValueChoosers.getRandomSex(),
                RandomStringGenerator.createRandomDateAsString());
    }
    public UpdateCrocodilesRequest prepareUpdateCrocodile () {
        return new UpdateCrocodilesRequest(RandomStringGenerator.createRandomStringAlphabeticWithLen(6),
                ValueChoosers.getRandomSex(),
                RandomStringGenerator.createCurrentDateAsString());
    }
    public CrocodilesRequest prepareCreateNewUser() {
        return new CrocodilesRequest(RandomStringGenerator.createRandomStringAlphabeticWithLen(8),
                RandomStringGenerator.createRandomStringAlphabeticWithLen(8),
                RandomStringGenerator.createRandomStringAlphabeticWithLen(8),
                RandomStringGenerator.createRandomEmailWithPrefixLen(6),
                RandomStringGenerator.createRandomStringAlphanumericWithLen(8));
    }
    public CreateNewCrocodileRequest prepareUpdateCrocodileName() {
        CreateNewCrocodileRequest createNewCrocodileRequest = new CreateNewCrocodileRequest();
        createNewCrocodileRequest.setName(RandomStringGenerator.createRandomStringAlphabeticWithLen(6));
        return createNewCrocodileRequest;
    }
    public CreateNewCrocodileRequest prepareUpdateCrocodileSex() {
        CreateNewCrocodileRequest createNewCrocodileRequest = new CreateNewCrocodileRequest();
        createNewCrocodileRequest.setSex(ValueChoosers.getRandomSex());
        return createNewCrocodileRequest;
    }
    public CreateNewCrocodileRequest prepareUpdateCrocodileDateOfBirth() {
        CreateNewCrocodileRequest createNewCrocodileRequest = new CreateNewCrocodileRequest();
        createNewCrocodileRequest.setDateOfBirth(RandomStringGenerator.createRandomDateAsString());
        return createNewCrocodileRequest;
    }

    public String prepareCreateToken(){
        TokenResponse token = CrocodilesAPI.createToken(new TokenRequest(ConfigSetup.getCrocodilesUsername(), ConfigSetup.getCrocodilePassword()));
        return token.getAccess();
    }
    public static int calculateAge(LocalDate dateOfBirth){
        int ages = (int) ChronoUnit.YEARS.between(dateOfBirth, LocalDate.now());
        return ages;
    }
}
