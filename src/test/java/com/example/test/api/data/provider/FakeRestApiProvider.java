package com.example.test.api.data.provider;

import com.example.test.api.common.RandomStringGenerator;
import com.example.test.api.constants.DataProviderNames;
import com.example.test.api.data.model.fakerestapi.ActivityRequest;
import org.testng.annotations.DataProvider;

public class FakeRestApiProvider {
    @DataProvider(name = DataProviderNames.VERIFY_NEW_ACTIVITY)
    public Object[][] verifyNewActivity(){
        return new Object[][]{
                {"firstActivity",this.prepareCreateActivity()},
                {"secondActivity",this.prepareCreateActivity()}
        };
    }
    @DataProvider(name = DataProviderNames.VERIFY_UPDATING_ACTIVITY)
    public Object[][] verifyUpdatingActivity(){
        return new Object[][]{
                {"First activity", this.prepareCreateActivity(), this.prepareCreateActivity()},
                {"Second activity", this.prepareCreateActivity(), this.prepareCreateActivity()}
        };
    }
    @DataProvider(name = DataProviderNames.VERIFY_DELETE_ACTIVITY)
    public Object[][] verifyDeleteActivity(){
        return new Object[][]{
                {"FirstDeletedActivity", this.prepareCreateActivity()},
                {"SecondDeletedActivity", this.prepareCreateActivity()}
        };
    }

    public ActivityRequest prepareCreateActivity(){
        return new ActivityRequest(RandomStringGenerator.createRandomWholeNumberInteger(1,10), RandomStringGenerator.createRandomStringAlphabeticWithLen(6), RandomStringGenerator.createCurrentDateTimeAsString(), RandomStringGenerator.createRandomBool());
    }


}
