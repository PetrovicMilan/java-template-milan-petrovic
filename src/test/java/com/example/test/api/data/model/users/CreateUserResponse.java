package com.example.test.api.data.model.users;

import com.example.test.api.annotations.ResponseRequiredField;
import com.example.test.api.data.model.common.ErrResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CreateUserResponse extends ErrResponse implements Serializable {

    @SerializedName("name")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String name;
    @SerializedName("job")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String job;
    @SerializedName("id")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String id;
    @SerializedName("createdAt")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String createdAt;

    public CreateUserResponse() {
    }

    public CreateUserResponse(String name, String job, String id, String createdAt) {
        super();
        this.name = name;
        this.job = job;
        this.id = id;
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void parseCreateUserResponse(CreateUserRequest createUserRequest){
            setJob(createUserRequest.getJob());
            setName(createUserRequest.getName());

    }
    public void parseUpdateUserResponse(CreateUserRequest createUserRequest){
        if(createUserRequest.getJob() != null){
            setJob(createUserRequest.getJob());
        }
        else if(createUserRequest.getName() != null) {
            setName(createUserRequest.getName());
        }
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(CreateUserResponse.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("name");
        sb.append('=');
        sb.append(((this.name == null)?"<null>":this.name));
        sb.append(',');
        sb.append("job");
        sb.append('=');
        sb.append(((this.job == null)?"<null>":this.job));
        sb.append(',');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null)?"<null>":this.id));
        sb.append(',');
        sb.append("createdAt");
        sb.append('=');
        sb.append(((this.createdAt == null)?"<null>":this.createdAt));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}