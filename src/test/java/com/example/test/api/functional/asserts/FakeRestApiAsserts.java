package com.example.test.api.functional.asserts;

import com.example.test.api.data.model.fakerestapi.ActivityResponse;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class FakeRestApiAsserts {
    private SoftAssert softAssert;
    public FakeRestApiAsserts(){this.softAssert = new SoftAssert();}
    public void assertCreateNewActivity(ActivityResponse actualResponse, ActivityResponse expectedResponse){
        if(actualResponse == null){
            Assert.fail("Response was null");
        }
        this.softAssert.assertEquals(actualResponse.getId(), expectedResponse.getId(), "Id did not match");
        this.softAssert.assertEquals(actualResponse.getCompleted(), expectedResponse.getCompleted(), "Completed did not match");
        this.softAssert.assertEquals(actualResponse.getTitle(), expectedResponse.getTitle(), "Title did not match");
        this.softAssert.assertNotNull(actualResponse.getDueDate(), "Due date was null");
        this.softAssert.assertAll();
    }
    public void assertUpdateActivity(ActivityResponse actualResponse, ActivityResponse expectedResponse){
        if(actualResponse == null){
            Assert.fail("Response was null");
        }
        this.softAssert.assertEquals(actualResponse.getId(), expectedResponse.getId(), "Id did not match");
        this.softAssert.assertEquals(actualResponse.getCompleted(), expectedResponse.getCompleted(), "Completed did not match");
        this.softAssert.assertEquals(actualResponse.getTitle(), expectedResponse.getTitle(), "Title did not match");
        this.softAssert.assertNotNull(actualResponse.getDueDate(), "Due date was null");
        this.softAssert.assertAll();
    }
    }

