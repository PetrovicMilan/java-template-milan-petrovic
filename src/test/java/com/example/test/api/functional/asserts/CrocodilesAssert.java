package com.example.test.api.functional.asserts;

import com.example.test.api.data.model.crocodiles.CreateCrocodileResponse;
import com.example.test.api.data.model.crocodiles.RegisterCrocodileResponse;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class CrocodilesAssert {
    private SoftAssert softAssert;
    public CrocodilesAssert(){this.softAssert = new SoftAssert(); }

    public void assertRegisterNewUser(RegisterCrocodileResponse actualCrocodileResponse, RegisterCrocodileResponse expectedCrocodileResponse){
        if(actualCrocodileResponse == null){
            Assert.fail("Response was null");
        }
        this.softAssert.assertEquals(actualCrocodileResponse.getEmail(), expectedCrocodileResponse.getEmail(), "Email did not match");
        this.softAssert.assertEquals(actualCrocodileResponse.getFirstName(), expectedCrocodileResponse.getFirstName(), "First name did not match");
        this.softAssert.assertEquals(actualCrocodileResponse.getLastName(), expectedCrocodileResponse.getLastName(), "Last name did not match");
        this.softAssert.assertEquals(actualCrocodileResponse.getUsername(), expectedCrocodileResponse.getUsername(), "User name did not match");
        this.softAssert.assertAll();
    }

    public void assertCreateNewCrocodile(CreateCrocodileResponse actualCrocodileResponse, CreateCrocodileResponse expectedCrocodileResponse){
        if(actualCrocodileResponse == null){
            Assert.fail("Response was null");
        }
        this.softAssert.assertEquals(actualCrocodileResponse.getName(), expectedCrocodileResponse.getName(), "Name did not match,");
        this.softAssert.assertEquals(actualCrocodileResponse.getSex(), expectedCrocodileResponse.getSex(), "Sex did not match,");
        this.softAssert.assertEquals(actualCrocodileResponse.getDateOfBirth(), expectedCrocodileResponse.getDateOfBirth(), "Date of birth did not match,");
        this.softAssert.assertEquals(actualCrocodileResponse.getAge(), expectedCrocodileResponse.getAge(),"Age did not match");
        this.softAssert.assertNotNull(actualCrocodileResponse.getId(), "Id should not be null,");
        this.softAssert.assertAll();
    }

    public void assertUpdateSingleCrocodile(CreateCrocodileResponse actualCrocodileResponse, CreateCrocodileResponse expectedCrocodileResponse){
        if(actualCrocodileResponse == null){
            Assert.fail("Response was null");
        }
        this.softAssert.assertEquals(actualCrocodileResponse.getName(), expectedCrocodileResponse.getName(), "Name did not match,");
        this.softAssert.assertEquals(actualCrocodileResponse.getSex(), expectedCrocodileResponse.getSex(), "Sex did not match,");
        this.softAssert.assertEquals(actualCrocodileResponse.getDateOfBirth(), expectedCrocodileResponse.getDateOfBirth(), "Date of birth did not match,");
        this.softAssert.assertEquals(actualCrocodileResponse.getId(), expectedCrocodileResponse.getId(), "Id did not match");
        this.softAssert.assertEquals(actualCrocodileResponse.getAge(), expectedCrocodileResponse.getAge(), "Age did not match");
        this.softAssert.assertAll();
    }
    public void assertDeleteSingleCrocodile(CreateCrocodileResponse actualResponse) {
        this.softAssert.assertNull(actualResponse.getAge(), "Age not null,");
        this.softAssert.assertNull(actualResponse.getId(), "Id not null");
        this.softAssert.assertNull(actualResponse.getName(), "Name not null");
        this.softAssert.assertNull(actualResponse.getSex(), "Sex not null");
        this.softAssert.assertNull(actualResponse.getDateOfBirth(), "Date of birth not null");
        this.softAssert.assertAll();
    }
}
