package com.example.test.api.functional.suites;

import com.example.test.api.calls.UserListAPI;
import com.example.test.api.common.RandomStringGenerator;
import com.example.test.api.common.init.TestBase;
import com.example.test.api.constants.DataProviderNames;
import com.example.test.api.data.model.users.*;
import com.example.test.api.data.provider.UsersProvider;
import com.example.test.api.functional.asserts.UserListAssert;
import org.testng.annotations.Test;

public class UsersTests extends TestBase {
    @Test(dataProvider = DataProviderNames.VERIFY_CREATE_USER, dataProviderClass = UsersProvider.class)
    public void testCreateUser(String Suffix, CreateUserRequest createUserRequest){
        logStep("INFO:Creating single user, adding response to actual response");
        CreateUserResponse actualResponse = UserListAPI.createSingleUser(createUserRequest);
        logStep("PASS: Created single user, added response to actual response");
        logStep("INFO: Creating expected response by parsing it from the given request");
        CreateUserResponse expectedResponse = new CreateUserResponse();
        expectedResponse.parseCreateUserResponse(createUserRequest);
        logStep("PASS: Created expected response");
        logStep("INFO: Comparing actual and expected response");
        UserListAssert userListAssert = new UserListAssert();
        userListAssert.assertCreateUser(actualResponse, expectedResponse);
        logStep("PASS: Test passed!");
    }
    @Test(dataProvider = DataProviderNames.VERIFY_UPDATE_USER, dataProviderClass = UsersProvider.class)
    public void testUpdateUser(String Suffix, CreateUserRequest createUserRequest, Integer id){
        logStep("INFO:Updating single user, adding response to actual response");
        UpdateSingleUserResponse actualResponse = UserListAPI.updateSingleUser(createUserRequest, id);
        logStep("PASS: Updated single user, added response to actual response");
        logStep("INFO: Creating expected response by parsing it from the given request");
        UpdateSingleUserResponse expectedResponse = new UpdateSingleUserResponse();
        expectedResponse.parseCreateUserResponse(createUserRequest);
        logStep("PASS: Created expected response");
        logStep("INFO: Comparing actual and expected response");
        UserListAssert userListAssert = new UserListAssert();
        userListAssert.assertUpdateUser(actualResponse, expectedResponse);
        logStep("PASS: Test passed!");
    }
    @Test(dataProvider = DataProviderNames.VERIFY_PATCH_UPDATE_USER, dataProviderClass = UsersProvider.class)
    public void testPatchUpdateUser(String Suffix, CreateUserRequest createUserRequest, Integer id){
        logStep("INFO: Creating actual response");
        PatchSingleUserResponse actualResponse = UserListAPI.patchSingleUser(createUserRequest, id);
        logStep("PASS: Created actual response");
        logStep("INFO: Creating expected response by parsing it from the given request");
        PatchSingleUserResponse expectedResponse = new PatchSingleUserResponse();
        expectedResponse.parseCreateUserResponse(createUserRequest);
        logStep("PASS: Created expected response");
        logStep("INFO: Comparing actual and expected response");
        UserListAssert userListAssert = new UserListAssert();
        userListAssert.assertPatchUser(actualResponse, expectedResponse);
        logStep("PASS: Test passed!");
    }

    @Test(dataProvider = DataProviderNames.VERIFY_DELETE_USER, dataProviderClass = UsersProvider.class)
    public void testDeleteUser(String suffix, CreateUserRequest createUserRequest){
        logStep("INFO:Creating single user, adding response to actual response");
        CreateUserResponse createUser = UserListAPI.createSingleUser(createUserRequest);
        logStep("PASS: Created single user, added response to actual response");
        logStep("INFO: Delete single user created above");
        UserListAPI.deleteSingleUser(Integer.valueOf(createUser.getId()));
        logStep("PASS: User deleted");
        logStep("INFO: Creating actual response");
        SingleUserResponse actualResponse = UserListAPI.getSingleUser(Integer.valueOf(createUser.getId()));
        logStep("PASS: Created actual response");
        logStep("INFO: Comparing actual response with empty response");
        UserListAssert userListAssert = new UserListAssert();
        userListAssert.assertDeleteUser(actualResponse);
        logStep("PASS: Test passed!");
    }
}
