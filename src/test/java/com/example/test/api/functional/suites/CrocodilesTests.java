package com.example.test.api.functional.suites;

import com.example.test.api.calls.CrocodilesAPI;
import com.example.test.api.common.init.TestBase;
import com.example.test.api.constants.DataProviderNames;
import com.example.test.api.data.model.crocodiles.*;
import com.example.test.api.data.provider.CrocodileProvider;
import com.example.test.api.functional.asserts.CrocodilesAssert;
import org.testng.annotations.Test;

public class CrocodilesTests extends TestBase {

    @Test(dataProvider = DataProviderNames.VERIFY_REGISTER_NEW_USER, dataProviderClass = CrocodileProvider.class)
    public void testRegisterNewUser(String Suffix, CrocodilesRequest crocodilesRequest){
        logStep("INFO: Create/Register new user");
        RegisterCrocodileResponse actualResponse = CrocodilesAPI.registerNewCrocodileUser(crocodilesRequest);
        logStep("PASS: Created/Registered new user");
        logStep("INFO: Creating expected response");
        RegisterCrocodileResponse expectedResponse = new RegisterCrocodileResponse();
        expectedResponse.parseCreateUserResponse(crocodilesRequest);
        logStep("PASS: Created expected response");
        logStep("INFO: Comparing actual response with the one we expected");
        CrocodilesAssert crocodilesAssert = new CrocodilesAssert();
        crocodilesAssert.assertRegisterNewUser(actualResponse, expectedResponse);
        logStep("PASS: Compared actual response with the one we expected");
    }
    @Test(dataProvider = DataProviderNames.VERIFY_CREATE_CROCODILE, dataProviderClass = CrocodileProvider.class)
    public void testCreateNewCrocodile(String methodNameSuffix, CreateNewCrocodileRequest createNewCrocodileRequest, String accessToken){
        logStep("INFO: Create new crocodile");
        CreateCrocodileResponse actualResponse = CrocodilesAPI.createNewCrocodile(createNewCrocodileRequest,accessToken);
        logStep("PASS: Created new crocodile");
        logStep("INFO: Creating expected response");
        CreateCrocodileResponse expectedResponse = new CreateCrocodileResponse();
        expectedResponse.parseCreateNewCrocodile(createNewCrocodileRequest);
        logStep("PASS: Created expected response");
        logStep("Comparing actual response with the one we expected");
        CrocodilesAssert crocodilesAssert = new CrocodilesAssert();
        crocodilesAssert.assertCreateNewCrocodile(actualResponse, expectedResponse);
        logStep("PASS: Compared actual response with the one we expected");
    }

    @Test(dataProvider = DataProviderNames.VERIFY_UPDATE_CROCODILE, dataProviderClass = CrocodileProvider.class)
    public void testUpdateCrocodile(String methodNameSuffix, CreateNewCrocodileRequest createNewCrocodileRequest, String accessToken, UpdateCrocodilesRequest updateSingleCrocodile){
        logStep("INFO: Create new crocodile");
        CreateCrocodileResponse newCrocodile = CrocodilesAPI.createNewCrocodile(createNewCrocodileRequest, accessToken);
        logStep("PASS: Created new crocodile");
        logStep("INFO: Updating the crocodile created above");
        CreateCrocodileResponse actualResponse = CrocodilesAPI.updateSingleCrocodile(updateSingleCrocodile, accessToken, newCrocodile.getId());
        logStep("PASS: Updated the crocodile created above");
        logStep("INFO: Creating expected response");
        CreateCrocodileResponse expectedResponse = new CreateCrocodileResponse();
        expectedResponse.parseUpdateSingleCrocodile(updateSingleCrocodile, newCrocodile);
        logStep("PASS: Created expected response");
        logStep("INFO: Comparing actual response with the one we expected");
        CrocodilesAssert crocodilesAssert = new CrocodilesAssert();
        crocodilesAssert.assertUpdateSingleCrocodile(actualResponse, expectedResponse);
        logStep("PASS: Compared actual response with the one we expected");
    }
    @Test(dataProvider = DataProviderNames.VERIFY_PATCH_CROCODILE, dataProviderClass = CrocodileProvider.class)
    public void testUpdateCrocodileWithPatch(String Suffix, CreateNewCrocodileRequest createNewCrocodileRequest, String accessToken, CreateNewCrocodileRequest updateSingleCrocodile){
        logStep("INFO: Create new crocodile");
        CreateCrocodileResponse newCrocodile = CrocodilesAPI.createNewCrocodile(createNewCrocodileRequest, accessToken);
        logStep("PASS: Created new crocodile");
        logStep("INFO: Updating the crocodile created above");
        CreateCrocodileResponse actualResponse = CrocodilesAPI.updateSingleCrocodilePatch(updateSingleCrocodile, accessToken, newCrocodile.getId());
        logStep("PASS: Updated the crocodile created above");
        logStep("INFO: Creating expected response");
        CreateCrocodileResponse expectedResponse = new CreateCrocodileResponse();
        expectedResponse.parseUpdateSingleCrocodile(updateSingleCrocodile, newCrocodile.getId(), createNewCrocodileRequest);
        logStep("PASS: Created expected response");
        logStep("INFO: Comparing actual response with the one we expected");
        CrocodilesAssert crocodilesAssert = new CrocodilesAssert();
        crocodilesAssert.assertUpdateSingleCrocodile(actualResponse, expectedResponse);
        logStep("PASS: Compared actual response with the one we expected");
    }

    @Test(dataProvider = DataProviderNames.VERIFY_DELETE_CROCODILE, dataProviderClass = CrocodileProvider.class)
    public void testDeleteCrocodile(String methodNameSuffix, CreateNewCrocodileRequest createNewCrocodileRequest, String accessToken){
        logStep("INFO: Create new crocodile");
        CreateCrocodileResponse newCrocodile = CrocodilesAPI.createNewCrocodile(createNewCrocodileRequest, accessToken);
        logStep("PASS: Created new crocodile");
        logStep("INFO: Delete the crocodile created above");
        CrocodilesAPI.deleteSingleCrocodile(newCrocodile.getId(), accessToken);
        logStep("PASS: Deleted the crocodile created above");
        logStep("INFO: Creating actual response");
        CreateCrocodileResponse actualResponse = CrocodilesAPI.getSingleCrocodile(newCrocodile.getId(), accessToken);
        logStep("INFO: Created actual response");
        logStep("INFO: Checking if response is null");
        CrocodilesAssert crocodilesAssert = new CrocodilesAssert();
        crocodilesAssert.assertDeleteSingleCrocodile(actualResponse);
        logStep("PASS: Checked if response is null");
    }
}