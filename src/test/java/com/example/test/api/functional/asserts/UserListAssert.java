package com.example.test.api.functional.asserts;

import com.example.test.api.data.model.users.CreateUserResponse;
import com.example.test.api.data.model.users.PatchSingleUserResponse;
import com.example.test.api.data.model.users.SingleUserResponse;
import com.example.test.api.data.model.users.UpdateSingleUserResponse;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class UserListAssert {
    private SoftAssert softAssert;
    public UserListAssert(){
        this.softAssert = new SoftAssert();
    }

    public void assertCreateUser(CreateUserResponse actualUserResponse, CreateUserResponse expectedUserResponse){
        if(actualUserResponse == null){
            Assert.fail("Response was null");
        }
        this.softAssert.assertEquals(actualUserResponse.getName(), expectedUserResponse.getName(),"Name did not match");
        this.softAssert.assertEquals(actualUserResponse.getJob(), expectedUserResponse.getJob(), "Job did not match");
        this.softAssert.assertNotNull(actualUserResponse.getId(),"Id is null");
        this.softAssert.assertAll();
    }
    public void assertUpdateUser(UpdateSingleUserResponse actualUserResponse, CreateUserResponse expectedUserResponse){
        if(actualUserResponse == null){
            Assert.fail("Response was null");
        }
        this.softAssert.assertEquals(actualUserResponse.getName(), expectedUserResponse.getName(), "Name did not match");
        this.softAssert.assertEquals(actualUserResponse.getJob(), expectedUserResponse.getJob(), "Job did not match");
        this.softAssert.assertEquals(actualUserResponse.getId(), expectedUserResponse.getId(), "Id did not match");
        this.softAssert.assertAll();
    }
    public void assertPatchUser(PatchSingleUserResponse actualUserResponse, CreateUserResponse expectedUserResponse){
        if(actualUserResponse == null){
            Assert.fail("Response was null");
        }
        this.softAssert.assertEquals(actualUserResponse.getName(), expectedUserResponse.getName(), "Name did not match");
        this.softAssert.assertEquals(actualUserResponse.getJob(), expectedUserResponse.getJob(), "Job did not match");
        this.softAssert.assertEquals(actualUserResponse.getId(), expectedUserResponse.getId(), "Id did not match");
        this.softAssert.assertAll();
    }
    public void assertDeleteUser(SingleUserResponse singleUser){
       this.softAssert.assertNull(singleUser.getData());
       this.softAssert.assertAll();
    }
}
