package com.example.test.api.functional.suites;

import com.example.test.api.calls.ActivitiesAPI;
import com.example.test.api.common.init.TestBase;
import com.example.test.api.constants.DataProviderNames;
import com.example.test.api.data.model.fakerestapi.ActivityRequest;
import com.example.test.api.data.model.fakerestapi.ActivityResponse;
import com.example.test.api.data.provider.FakeRestApiProvider;
import com.example.test.api.functional.asserts.FakeRestApiAsserts;
import org.testng.annotations.Test;

public class FakeRestApiTests extends TestBase {
    @Test(dataProvider = DataProviderNames.VERIFY_NEW_ACTIVITY, dataProviderClass = FakeRestApiProvider.class)
    public void testCreateNewActivity(String Suffix, ActivityRequest activityRequest){
        logStep("INFO: Creating actual response");
        ActivityResponse actualResponse = ActivitiesAPI.createNewActivity(activityRequest);
        logStep("INFO: Created actual response");
        logStep("INFO: Creating expected response");
        ActivityResponse expectedResponse = new ActivityResponse();
        expectedResponse.parseCreateNewActivity(activityRequest);
        logStep("PASS: Created expected response");
        logStep("INFO: Compare actual response and expected response");
        FakeRestApiAsserts fakeRestApiAsserts = new FakeRestApiAsserts();
        fakeRestApiAsserts.assertCreateNewActivity(actualResponse, expectedResponse);
        logStep("PASS: Test passed!");
    }

    @Test(dataProvider = DataProviderNames.VERIFY_UPDATING_ACTIVITY, dataProviderClass = FakeRestApiProvider.class)
    public void testUpdateActivity(String Suffix, ActivityRequest activityRequest, ActivityRequest updatedRequest){
        logStep("INFO: Creating activity we should update");
        ActivityResponse newActivity = ActivitiesAPI.createNewActivity(activityRequest);
        logStep("PASS: Created activity we should update");
        logStep("INFO: Updating activity we made earlier");
        ActivityResponse actualResponse = ActivitiesAPI.updateExistingActivity(updatedRequest, newActivity.getId());
        logStep("PASS: Updated activity we made earlier, and created actual response");
        logStep("INFO: Creating expected response");
        ActivityResponse expectedResponse = new ActivityResponse();
        expectedResponse.parseCreateNewActivity(updatedRequest);
        logStep("PASS: Created expected response");
        logStep("INFO: Comparing actual response and expected response");
        FakeRestApiAsserts fakeRestApiAsserts = new FakeRestApiAsserts();
        fakeRestApiAsserts.assertUpdateActivity(actualResponse, expectedResponse);
        logStep("PASS: Test passed!");
    }
}