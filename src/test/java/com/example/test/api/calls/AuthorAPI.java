package com.example.test.api.calls;

import com.example.test.api.common.GsonFunctions;
import com.example.test.api.common.ResponseValidation;
import com.example.test.api.common.RestAssuredFunctions;
import com.example.test.api.constants.ApiEndpoints;
import com.example.test.api.data.model.common.EmptyResponse;
import com.example.test.api.data.model.fakerestapi.Author;
import com.example.test.api.data.model.fakerestapi.AuthorRequest;
import com.example.test.api.data.model.fakerestapi.AuthorResponse;

public class AuthorAPI {
    public static ResponseValidation verifyAuthors(){
        String jsonResponse = RestAssuredFunctions.get(ApiEndpoints.AUTHORS).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, Author.class);
    }
    public static AuthorResponse createNewAuthor(AuthorRequest authorRequest){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.post(authorRequest, ApiEndpoints.AUTHORS).body().asString(), AuthorResponse.class);
    }
    public static ResponseValidation verifyBookID(Integer ID) {
        String jsonResponse = RestAssuredFunctions.get(ApiEndpoints.AUTHOR_BOOK_ID(ID)).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, Author.class);
    }
    public static ResponseValidation verifySingleAuthor(Integer ID){
        String jsonResponse = RestAssuredFunctions.get(ApiEndpoints.AUTHOR_ID(ID)).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, Author.class);
    }
    public static AuthorResponse updateAuthor(AuthorRequest authorRequest) {
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.put(authorRequest, ApiEndpoints.AUTHOR_ID(authorRequest.getId())).body().asString(), AuthorResponse.class);
    }
    public static EmptyResponse deleteAuthor(Integer ID) {
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.delete(ApiEndpoints.AUTHOR_ID(ID)).body().asString(), EmptyResponse.class);
    }
}
