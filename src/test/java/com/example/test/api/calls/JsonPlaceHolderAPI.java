package com.example.test.api.calls;

import com.example.test.api.common.GsonFunctions;
import com.example.test.api.common.ResponseValidation;
import com.example.test.api.common.RestAssuredFunctions;
import com.example.test.api.constants.ApiEndpoints;
import com.example.test.api.data.model.jsonplaceholder.JsonPlaceHolderComment;
import com.example.test.api.data.model.jsonplaceholder.JsonPlaceHolderPosts;
import com.example.test.api.data.model.jsonplaceholder.JsonPlaceHolderRequests;
import com.example.test.api.data.model.jsonplaceholder.JsonPlaceHolderResponse;

public class JsonPlaceHolderAPI {
        public static ResponseValidation verifyJsonPlaceHolderPosts(){
            String jsonResponse = RestAssuredFunctions.get(ApiEndpoints.JSON_PLACEHOLDER_GETPOSTS).body().asString();
            return GsonFunctions.verifyResponse(jsonResponse, JsonPlaceHolderPosts.class);
        }
        public static ResponseValidation verifyJsonPlaceHolderSinglePost(Integer Id){
            String jsonResponse = RestAssuredFunctions.get(ApiEndpoints.JSON_PLACEHOLDER_GETSINGLEPOST(Id)).body().asString();
            return GsonFunctions.verifyResponse(jsonResponse, JsonPlaceHolderPosts.class);
        }
        public static ResponseValidation verifyJsonPlaceHolderSingleUserComments(Integer Id){
            String jsonResponse = RestAssuredFunctions.get(ApiEndpoints.JSON_PLACEHOLDER_GETSINGLEUSERSCOMMENTS(Id)).body().asString();
            return GsonFunctions.verifyResponse(jsonResponse, JsonPlaceHolderComment.class);
        }
        public static ResponseValidation verifyJsonPlaceHolderCommentsFromPostID(Integer postId) {
            String jsonResponse = RestAssuredFunctions.get(ApiEndpoints.JSON_PLACEHOLDER_GETCOMMENTSPOSTID(postId)).body().asString();
            return GsonFunctions.verifyResponse(jsonResponse, JsonPlaceHolderComment.class);
        }
        public static JsonPlaceHolderResponse jsonPlaceHolderCreatePost(JsonPlaceHolderRequests jsonPlaceHolderRequests) {
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.post(jsonPlaceHolderRequests, ApiEndpoints.JSON_PLACEHOLDER_GETPOSTS).body().asString(), JsonPlaceHolderResponse.class);
        }
}
