package com.example.test.api.calls;

import com.example.test.api.common.GsonFunctions;
import com.example.test.api.common.ResponseValidation;
import com.example.test.api.common.RestAssuredFunctions;
import com.example.test.api.constants.ApiEndpoints;
import com.example.test.api.data.model.common.EmptyResponse;
import com.example.test.api.data.model.crocodiles.*;

public class CrocodilesAPI {
    public static ResponseValidation validateListOfCrocodiles(){
        String jsonResponse = RestAssuredFunctions.get(ApiEndpoints.GET_LIST_OF_CROCODILES).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, ListCrocodile.class);
    }
    public static ResponseValidation validateSingleCrocodile(Integer id){
        String jsonResponse = RestAssuredFunctions.get(ApiEndpoints.GET_SINGLE_CROCODILE(id)).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, ListCrocodile.class);
    }
    public static RegisterCrocodileResponse registerNewCrocodileUser(CrocodilesRequest crocodilesRequest){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.post(crocodilesRequest, ApiEndpoints.REGISTER_CROCODILE_USER).body().asString(), RegisterCrocodileResponse.class);
    }
    public static CreateCrocodileResponse createNewCrocodile(CreateNewCrocodileRequest createNewCrocodileRequest, String tokenResponse){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.post(createNewCrocodileRequest, tokenResponse, ApiEndpoints.GET_AND_CREATE_CROCODILES).body().asString(), CreateCrocodileResponse.class);
    }
    public static GetSingleCrocodileResponse getSingleCrocodile(Integer id, String accessToken){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.get(accessToken, ApiEndpoints.GET_SINGLE_CROCODILE(id)).body().asString(), GetSingleCrocodileResponse.class);
    }
    public static UpdateCrocodileResponse updateSingleCrocodile(UpdateCrocodilesRequest updateCrocodileRequest, String token, Integer id){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.put(updateCrocodileRequest, token, ApiEndpoints.UPDATE_AND_DELETE_SINGLE_CROCODILE(id)).body().asString(), UpdateCrocodileResponse.class);
    }
    public static UpdateCrocodileResponse updateSingleCrocodilePatch(CreateNewCrocodileRequest updateCrocodileRequest, String token, Integer id){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.patch(updateCrocodileRequest, token, ApiEndpoints.UPDATE_AND_DELETE_SINGLE_CROCODILE(id)).body().asString(), UpdateCrocodileResponse.class);
    }
    public static TokenResponse createToken(TokenRequest tokenRequest){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.post(tokenRequest, ApiEndpoints.LOGIN_TOKEN).body().asString(), TokenResponse.class);
    }
    public static EmptyResponse deleteSingleCrocodile(Integer id, String token){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.delete(token, ApiEndpoints.UPDATE_AND_DELETE_SINGLE_CROCODILE(id)).body().asString(), EmptyResponse.class);
    }
}
