package com.example.test.api.calls;

import com.example.test.api.common.GsonFunctions;
import com.example.test.api.common.ResponseValidation;
import com.example.test.api.common.RestAssuredFunctions;
import com.example.test.api.constants.ApiEndpoints;
import com.example.test.api.data.model.common.EmptyResponse;
import com.example.test.api.data.model.fakerestapi.Activity;
import com.example.test.api.data.model.fakerestapi.ActivityRequest;
import com.example.test.api.data.model.fakerestapi.ActivityResponse;

import javax.persistence.criteria.CriteriaBuilder;

public class ActivitiesAPI {
    public static ResponseValidation getAllActivities(){
        String jsonResponse = RestAssuredFunctions.get(ApiEndpoints.FAKE_REST_API_ACTIVITIES).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, Activity.class);
    }
    public static ActivityResponse createNewActivity(ActivityRequest activityRequest) {
    return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.post(activityRequest, ApiEndpoints.FAKE_REST_API_ACTIVITIES).body().asString(), ActivityResponse.class);
}
    public static ResponseValidation getActivitiesDefinedID(Integer id){
        String jsonResponse = RestAssuredFunctions.get(ApiEndpoints.FAKE_REST_API_ACTIVITIESID(id)).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, ActivityResponse.class);
    }
    public static ActivityResponse getSingleActivity(Integer id) {
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.get(ApiEndpoints.FAKE_REST_API_ACTIVITIESID(id)).body().asString(), ActivityResponse.class);
    }
    public static ActivityResponse updateExistingActivity(ActivityRequest activityRequest, Integer id){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.put(activityRequest, ApiEndpoints.FAKE_REST_API_ACTIVITIESID(id)).body().asString(), ActivityResponse.class);
    }
    public static EmptyResponse deletingActivity(Integer ID){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.delete(ApiEndpoints.FAKE_REST_API_ACTIVITIESID(ID)).body().asString(), EmptyResponse.class);
    }
}
