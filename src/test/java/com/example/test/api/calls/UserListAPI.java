package com.example.test.api.calls;

import com.example.test.api.common.GsonFunctions;
import com.example.test.api.common.ResponseValidation;
import com.example.test.api.common.RestAssuredFunctions;
import com.example.test.api.constants.ApiEndpoints;
import com.example.test.api.data.model.common.EmptyResponse;
import com.example.test.api.data.model.users.*;

public class UserListAPI {
    public static ResponseValidation validateGetListOfUsers(){
        String jsonResponse = RestAssuredFunctions.get(ApiEndpoints.GET_LIST_OF_USERS).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, UserList.class);
    }
    public static ResponseValidation validateGetSingleUser(Integer id){
        String jsonResponse = RestAssuredFunctions.get(ApiEndpoints.GET_AND_DELETE_SINGLE_USER(id)).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, SingleUserResponse.class);
    }
    public static ResponseValidation validateListOfResources(){
        String jsonResponse = RestAssuredFunctions.get(ApiEndpoints.GET_LIST_OF_RESOURCES).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, ListResource.class);
    }
    public static ResponseValidation validateSingleResource(){
    String jsonResponse = RestAssuredFunctions.get(ApiEndpoints.GET_ONE_RESOURCE).body().asString();
    return GsonFunctions.verifyResponse(jsonResponse, SingleResource.class);
    }
    public static CreateUserResponse createSingleUser(CreateUserRequest createUserRequest){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.post(createUserRequest, ApiEndpoints.CREATE_SINGLE_USER).body().asString(), CreateUserResponse.class);
    }
    public static UpdateSingleUserResponse updateSingleUser(CreateUserRequest createUserRequest, Integer Id){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.put(createUserRequest, ApiEndpoints.UPDATE_USER(Id)).body().asString(), UpdateSingleUserResponse.class);
    }
    public static PatchSingleUserResponse patchSingleUser(CreateUserRequest createUserRequest, Integer Id){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.patch(createUserRequest, ApiEndpoints.UPDATE_USER(Id)).body().asString(), PatchSingleUserResponse.class);
    }
    public static RegistrationResponse registrationRequest(RegistrationRequest registrationRequest){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.post(registrationRequest, ApiEndpoints.REGISTER_SUCCESSFUL).body().asString(), RegistrationResponse.class);
    }
    public static LoginResponse loginResponse(LoginRequest loginRequest){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.post(loginRequest, ApiEndpoints.LOGIN_SUCCESSFUL).body().asString(), LoginResponse.class);
    }
    public static EmptyResponse deleteSingleUser(Integer Id){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.delete(ApiEndpoints.GET_AND_DELETE_SINGLE_USER(Id)).body().asString(), EmptyResponse.class);
    }
    public static SingleUserResponse getSingleUser(Integer Id){
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.get(ApiEndpoints.GET_AND_DELETE_SINGLE_USER(Id)).body().asString(), SingleUserResponse.class);
    }
}
