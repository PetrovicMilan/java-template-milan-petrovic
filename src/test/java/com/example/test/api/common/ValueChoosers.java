package com.example.test.api.common;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Random;

public class ValueChoosers {
    public static String getRandomSex(){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("M");
        arrayList.add("F");
        Random random = new Random();
        return arrayList.get(random.nextInt(arrayList.size()));
    }
    public static String getRandomPosition(){
        ArrayList<String> positionsList = new ArrayList<>();
        positionsList.add("QA");
        positionsList.add("FE");
        positionsList.add("BE");
        positionsList.add("SM");
        Random random = new Random();
        return positionsList.get(random.nextInt(positionsList.size()));
    }
}
